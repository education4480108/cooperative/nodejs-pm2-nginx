nodejs + pm2 + nginx

Структура

myproject/
├── docker-compose.yml
├── nginx/
│   └── nginx.conf
├── nodejs/
│   ├── Dockerfile
│   ├── package.json
│   └── app.js

Запустить проект cd /myproject/ затем docker compose up --build

Открываем или курлим адрес машины с докером localhost:80 это nginx localhost:3000 напрямую мимо nginx
